import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatTooltipModule} from '@angular/material';
import {PARecord} from '../_models/PARecord';
import {NotificationService} from '../_services/notification.service';
import {UserService} from '../_services/user.service';
import {User} from "../_models/user";


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'parecord-component',
  templateUrl: './parecord.component.html',
  styleUrls: ['./parecord.component.css']
})
export class ParecordComponent implements OnInit {
  @Input() parecord: PARecord;
  @Output() deleteEvent = new EventEmitter<Date>();

   mode = 'determinate';

   message = '';

   bufferValue = 0;

   activities = ['directions_walk', 'directions_run', 'directions_bike'];


   color = 'primary';

   activity = this.activities[0];
   calprogressvalue = 0;
   minprogressvalue = 0;

  constructor(private notifService: NotificationService, private userService: UserService) { }

  delete(date) {
    console.log('date to delete = ', date);
    this.deleteEvent.emit(date);
  }

  notImplemented(message) {

    this.notifService.notImplementedWarning(message);
  }

  ngOnInit() {
    this.activity = this.activities[this.parecord.activityType];



    this.userService.getGoals(this.parecord.createdBy).subscribe(
      goals => {
        this.calprogressvalue = Math.floor(this.parecord.calories / goals[0].caloriegoal * 100);
        this.minprogressvalue = Math.floor(this.parecord.minutes / goals[0].minutegoal * 100);
      },
      error => {
        this.notifService.showNotif(error.toString(), 'warning'); });




  //
    //
  }


}
