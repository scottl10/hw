import { Component, OnInit } from '@angular/core';
import {UserService} from '../_services/user.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  myModel = 0;

  myModel2 = 0;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  saveGoals() {
    const goals = {caloriegoal: this.myModel, minutegoal: this.myModel2};
    this.userService.setGoals(goals);
  }

}
